from django.shortcuts import render, redirect, get_object_or_404
from todos.models import ToDoList, ToDoItem
from todos.forms import ToDoListForm, ToDoItemForm

# Create your views here.
################################################################
#My view of different lists

def to_do_list(request):
    lists = ToDoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/lists.html", context)

###################################################################
#View of chores within My Lists

def todo_list_detail(request, id):
    list_detail = ToDoList.objects.get(id=id)
    context = {
        "list_detail": list_detail,
     }
    return render(request, "todos/detail.html", context)

######################################################################
# Creating new to do lists

def todo_list_create(request):
    if request.method == "POST":
        form = ToDoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ToDoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

#########################################################################
#Editing chores in lists

def todo_list_update(request, id):
    list = ToDoList.objects.get(id=id)
    if request.method == "POST":
        form = ToDoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ToDoListForm(instance=list)
    context = {
            "form": form,
        }
    return render(request, "todos/update.html", context)

###########################################################################
#Delete a To Do List:

def todo_list_delete(request, id):
    list = ToDoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_detail")
    return render(request, "todos/delete.html")

###########################################################################
#Add Chore to todo list

def todo_item_create(request):
    if request.method =="POST":
        form = ToDoItemForm(request.POST)
        if form.is_valid():
            list_item = form.save()
            return redirect("todo_list_detail", id=list_item.list_detail.id)
    else:
        form = ToDoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)
###########################################################################
#ToDoItem Update

def todo_item_update(request, id):
    item = ToDoItem.objects.get(id=id)
    if request.method == "POST":
        form = ToDoItemForm(request.POST, instance=item)
        if form.is_valid():
            list_item = form.save()
            return redirect("todo_list_detail", id=list_item.list_detail.id)
    else:
        form = ToDoItemForm(instance=item)
    context = {
        "form": form,
    }
    return render(request, "todos/itemupdate.html", context)
