from django.forms import ModelForm
from todos.models import ToDoList, ToDoItem

class ToDoListForm(ModelForm):
    class Meta:
        model = ToDoList
        fields = [
            "name",
        ]


class ToDoItemForm(ModelForm):
    class Meta:
        model = ToDoItem
        fields = "__all__"

   